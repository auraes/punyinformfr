! ==============================================================================
! Lionel Ange, 2008-2024
! Version 1.1.0
! Licence Creative Commons BY-SA 4.0 (CC BY-SA 4.0)
! https://creativecommons.org/licenses/by-sa/4.0/
! ==============================================================================
! Source encoding UTF-8 No Mark
System_file;

Array StorageForShortName buffer 12; !FIXME Warning : valeur à ne pas dépasser

#IfnDef LanguageArticles;
Array LanguageArticles static -->
    "La "  "la "  "une "   ! 0 pomme
    "Le "  "le "  "un "    ! 1 dé
    "Les " "les " "des "   ! 2 lunettes
    "La "  "la "  "de la " ! 3 confiture (en)
    "Le "  "le "  "du "    ! 4 sucre     (en)
    "L'"   "l'"   "une "   ! 5 orange
    "L'"   "l'"   "un "    ! 6 œuf
    "L'"   "l'"   "de l'"  ! 7 eau
!    ""     ""     ""      ! 8 Julia
;!End_Array
#Endif;

Array vowel static -> !ï ÿ Ÿ æ Æ è È
    'a'  'e'  'h'  'i'  'o'  'u' 'â'  'é'  'ê'  'î'  'ô'  'œ'
    'A'  'E'  'H'  'I'  'O'  'U' 'Â'  'É'  'Ê'  'Î'  'Ô'  'Œ';

[ elide w;
   @output_stream 3 StorageForShortName;
   print (name) w;
   @output_stream -3;
   w = LanguageContraction((StorageForShortName + WORDSIZE)->0);
   return w;
];

[ LanguageContraction c   i; !WARNING z5
    @scan_table c vowel $18 $01 -> i ?rtrue;
    rfalse;
];

[ LanguagePronom w;
   switch (w) {
      '-l^':   print "l'";
      '-le':   print "le";
      '-la':   print "la";
      '-les':  print "les";
      'lui':   print "lui";
      'leur':  print "leur";
   }
];

! From library 6.12
! To protect against a bug in early versions of the "Zip" interpreter:
[ Tokenise__ b p; b->(2 + b->1) = 0; @tokenise b p; ];

! From library 6.12
[ LTI_Insert i ch  b y;

    ! Protect us from strict mode, as this isn't an array in quite the
    ! sense it expects
    b = buffer;

    ! Insert character ch into buffer at point i.
    ! Being careful not to let the buffer possibly overflow:
    y = b->1;
    if (y > b->0) y = b->0;

    ! Move the subsequent text along one character:
    for (y=y+2 : y>i : y--) b->y = b->(y-1);
    b->i = ch;
    ! And the text is now one character longer:
    if (b->1 < b->0) (b->1)++;
];

[ PrintToBuffer str;
    @output_stream 3 StorageForShortName;
    print (string) str;
    @output_stream -3;
    return StorageForShortName-->0; !Warning si débordement
];

[ PosMot p_wn;
    @log_shift p_wn (2) -> p_wn;
    ++p_wn;
    return (parse->p_wn);
];

[ DicMot p_wn;
    @log_shift p_wn (1) -> p_wn;
    --p_wn;
    return (parse-->p_wn);
];

[ NbMotPhrase   p_wn i w;
    i = parse->1;
    for (p_wn = 1 : p_wn <= i : p_wn++) {
        w = DicMot(p_wn);
        if (w == './/' or THEN1__WD)
            break;
    }
    --p_wn;
    return p_wn;
];

[ EffaceMot p_wn   at lgm;
    at = PosMot(p_wn);
    lgm = WordLength(p_wn) + at;
    while (at < lgm)
        buffer->at++ = ' ';
];

[ InsertMot p_wn s   at i lgc; ! p_wn de 1 à NbrMotPhrase() + 1
    i = NbMotPhrase();
    if (p_wn > i) {
        p_wn = i;
        at = WordLength(p_wn);
    }
    at = at + PosMot(p_wn);
    LTI_Insert(at++, ' ');
    lgc = PrintToBuffer(s) + at;
    i = WORDSIZE;
    while (at < lgc)
        LTI_Insert(at++, StorageForShortName->(i++));
    LTI_Insert(at, ' ');
];

[ DecolleApostrophe p_wn   at lgm; !TODO ssi s', l' ou d' ?
    at = PosMot(p_wn) + 1;
    lgm = WordLength(p_wn);
    if (lgm > 1 && buffer->at == ''') {
        LTI_Insert(++at, ' ');
        rtrue;
    }
    rfalse;
];

[ Translation   n i w1 w2 r;
    n = NbMotPhrase();
    for (i=1: i<=n: i++) {
        w1 = DicMot(i);
        if (w1 == 0) { ! WARNING si >= 9 ou 6
            r = DecolleApostrophe(i);
            if (r) {
                ++n;
                Tokenise__(buffer, parse);
            }
        }
    }
    if (n == 1)
        return;
    w2 = parse-->3;
    if (n == 2 && ((w2->#dict_par1) & $01 == 0))
        return;
    if (n == 3 && (((parse-->5)->#dict_par1) & $01 == 0))
        return;
    w1 = parse-->1;
    r = 1;
    switch (w1) { !y en tout
        'se', 's^': w1 = "soi";
        'l^': w1 = "-l'";
        'le':
            switch (w2) {
                'lui':    w1 = "lui -le"; r = 2;
                'leur':   w1 = "leur -le"; r = 2;
                default:  w1 = "-le";
            }
        'la':
            switch (w2) {
                'lui':    w1 = "lui -la"; r = 2;
                'leur':   w1 = "leur -la"; r = 2;
                default:  w1 = "-la";
            }
        'les':
            switch (w2) {
                'lui':    w1 = "lui -les"; r = 2;
                'leur':   w1 = "leur -les"; r = 2;
                default:  w1 = "-les";
            }
        'lui':  w1 = "lui";  !"à lui"
        'leur': w1 = "leur"; !"à eux"
        default: r = 0;
    }
    if (r) {
        EffaceMot(1);
        if (r == 1)
            InsertMot(3, w1);
        else {
            EffaceMot(2);
            InsertMot(4, w1);
        }
        Tokenise__(buffer, parse);
    }
];
