! Part of PunyInform: A small stdlib and parser for interactive fiction
! suitable for old-school computers such as the Commodore 64.
! Designed to be similar, but not identical, to the Inform 6 library.
!
System_file;

!
! Simple string messages
!

#IfDef MSG_OK;
Constant MSG_TAKE_DEFAULT MSG_OK;
Constant MSG_DROP_DROPPED MSG_OK;
Constant MSG_REMOVE_DEFAULT MSG_OK;
!Constant MSG_SAVE_DEFAULT MSG_OK;
#EndIf;

#Ifndef MSG_TAKE_YOURSELF;
Constant MSG_TAKE_YOURSELF
    "Vous vous êtes toujours appartenu.";
#EndIf;
#Ifndef MSG_TAKE_NO_CAPACITY;
Constant MSG_TAKE_NO_CAPACITY
    "Vous avez déjà bien assez de choses.";
    !transportez/trop/suffisamment/d'objets
#EndIf;
#Ifndef MSG_TAKE_DEFAULT;
Constant MSG_TAKE_DEFAULT
    "C'est pris.";
#EndIf;
#Ifndef MSG_DRINK_NOTHING_SUITABLE;
Constant MSG_DRINK_NOTHING_SUITABLE
    "Il n'y a rien de vraiment buvable ici.";
#EndIf;
#Ifndef MSG_DROP_DROPPED;
Constant MSG_DROP_DROPPED
    "C'est posé.";
#EndIf;
#Ifndef MSG_THROW_ANIMATE;
Constant MSG_THROW_ANIMATE
    "C'est sans grand intérêt.";
    !inutile futile
#Endif;
#Ifndef MSG_THROW_DEFAULT;
Constant MSG_THROW_DEFAULT
    "Vous renoncez au dernier moment.";
#Endif;
#Ifndef MSG_SMELL_DEFAULT;
Constant MSG_SMELL_DEFAULT
    "Vous ne sentez rien de particulier.";
    !inhabituel spécial inattendu
#Endif;
#Ifndef MSG_LISTEN_DEFAULT;
Constant MSG_LISTEN_DEFAULT
    "Vous n'entendez rien de particulier.";
    !inhabituel spécial inattendu
#Endif;
#Ifndef MSG_TELL_PLAYER;
Constant MSG_TELL_PLAYER
    "Vous vous dites quelques mots.";
#Endif;
#Ifndef MSG_TELL_DEFAULT;
Constant MSG_TELL_DEFAULT
    "Cela ne provoque aucune réaction.";
#Endif;
#Ifndef MSG_ENTER_ALREADY;
Constant MSG_ENTER_ALREADY
    "Vous y êtes déjà.";
#Endif;
#Ifndef MSG_EXIT_ALREADY;
Constant MSG_EXIT_ALREADY
    "Vous n'êtes dans rien de particulier.";
    !pour l'instant.
#Endif;
#Ifndef MSG_EXIT_NOT_ON;
Constant MSG_EXIT_NOT_ON
    "Vous n'y êtes pas pour l'instant."; !FIXME
    !Vous n'êtes pas (là-)dessus./n'y êtes pas.
#Endif;
#Ifndef MSG_EXIT_NOT_IN;
Constant MSG_EXIT_NOT_IN
    "Vous n'y êtes pas pour l'instant."; !FIXME
    !Vous n'êtes pas (là-)dedans./n'y êtes pas.
#Endif;
#Ifndef MSG_INVENTORY_EMPTY;
Constant MSG_INVENTORY_EMPTY
    "Vous n'avez rien avec vous.";
#Endif;
#Ifndef MSG_GO_CANT_GO;
Constant MSG_GO_CANT_GO
    "Vous ne pouvez pas aller par là.";
#Endif;
#Ifndef MSG_SAVE_DEFAULT;
Constant MSG_SAVE_DEFAULT
    "C'est sauvegardé.";
#Endif;
#Ifndef MSG_INSERT_ITSELF;
Constant MSG_INSERT_ITSELF
    "Vous ne pouvez pas mettre quelque chose dans lui-même.";
#Endif;
#Ifndef MSG_PUTON_ITSELF;
Constant MSG_PUTON_ITSELF
    "Vous ne pouvez pas mettre quelque chose sur lui-même.";
#Endif;
#Ifndef MSG_ATTACK_DEFAULT;
Constant MSG_ATTACK_DEFAULT
    "La violence n'est pas toujours la solution.";
    !bonne/appropriée
    !"La violence commence là ou la parole s'arrête.";
#Endif;
#Ifndef MSG_FILL_NO_WATER;
Constant MSG_FILL_NO_WATER
    "Il n'y a rien de vraiment approprié.";
#EndIf;
#Ifndef MSG_DIG_NO_USE;
Constant MSG_DIG_NO_USE
    "Cela n'aboutirait pas à grand-chose.";
    !Creuser ici
#EndIf;
#Ifndef MSG_WAIT_DEFAULT;
Constant MSG_WAIT_DEFAULT
    "Il ne se passe rien de particulier.";
#EndIf;
#Ifndef MSG_TOUCH_DEFAULT;
Constant MSG_TOUCH_DEFAULT
    "Vous ne ressentez rien de particulier.";
    !inhabituel spécial inattendu
#EndIf;
#Ifndef MSG_PUSHDIR_DEFAULT;
Constant MSG_PUSHDIR_DEFAULT
    "Vous ne pouvez rien envisager d'autre ?";
    !Essayez autre chose
#EndIf;
#Ifndef MSG_JUMP;
Constant MSG_JUMP
    "Vous sautez sur place, en vain."; !vainement
#EndIf;
#Ifndef MSG_REMOVE_DEFAULT;
Constant MSG_REMOVE_DEFAULT
    "C'est retiré";
#EndIf;
#Ifndef MSG_SEARCH_NOTHING_SPECIAL;
Constant MSG_SEARCH_NOTHING_SPECIAL
    "Vous ne trouvez rien de particulièrement intéressant.";
    !de particulier/spécial
#EndIf;
#Ifndef MSG_OOPS_DEFAULT;
Constant MSG_OOPS_DEFAULT
    "N'y pensez même pas.";
#EndIf;
#Ifndef MSG_PARSER_ONLY_TO_ANIMATE;
Constant MSG_PARSER_ONLY_TO_ANIMATE
    "Vous ne pouvez faire cela qu'avec un être animé.";
    !personne ou animal/chose personnifiée/le faire
#EndIf;
#Ifndef MSG_PARSER_NOT_MULTIPLE_VERB;
Constant MSG_PARSER_NOT_MULTIPLE_VERB
    "Vous ne pouvez pas utiliser plusieurs objets avec ce verbe";
#EndIf;
#Ifndef MSG_PARSER_NOT_MULTIPLE_DIRS;
Constant MSG_PARSER_NOT_MULTIPLE_DIRS
    "Vous ne pouvez pas indiquer plusieurs directions à la fois.";
#EndIf;
#Ifndef MSG_PARSER_BAD_NUMBER;
Constant MSG_PARSER_BAD_NUMBER
    "Je n'ai pas compris ce nombre.";
#EndIf;
#Ifndef MSG_PARSER_NO_INPUT;
Constant MSG_PARSER_NO_INPUT
    "Redites-moi ça ?";
    !Et quoi d'autre !/ Rien à faire !
#EndIf;
#Ifndef MSG_PARSER_UNKNOWN_SENTENCE;
Constant MSG_PARSER_UNKNOWN_SENTENCE
    "Je n'ai pas compris cette phrase.";
    !votre commande
#EndIf;
#Ifndef MSG_PARSER_UNKNOWN_VERB;
Constant MSG_PARSER_UNKNOWN_VERB
    "Je ne reconnais pas ce verbe.";
#EndIf;
#Ifndef MSG_PARSER_CANT_DISAMBIGUATE;
Constant MSG_PARSER_CANT_DISAMBIGUATE
    "Je ne comprends toujours pas à quoi vous faites référence.";
#EndIf;
#Ifndef MSG_PARSER_UNKNOWN_PERSON;
Constant MSG_PARSER_UNKNOWN_PERSON
    "Je ne comprends pas à qui vous faites référence.";
#EndIf;
#Ifndef MSG_PARSER_NOSUCHTHING;
Constant MSG_PARSER_NOSUCHTHING
    "Vous ne pouvez voir ou interagir avec cela.";
    !ça/Vous ne voyez rien de tel.
#EndIf;
#Ifndef MSG_PARSER_CANT_OOPS;
Constant MSG_PARSER_CANT_OOPS
    "Désolé, cela ne peut pas être corrigé.";
#EndIf;
#Ifndef MSG_PARSER_COMPLEX_AGAIN;
Constant MSG_PARSER_COMPLEX_AGAIN
    "La commande « encore » doit se trouver sur une nouvelle ligne de saisie.^";
    !commande
#EndIf;
#Ifndef MSG_PARSER_NOTHING_TO_AGAIN;
Constant MSG_PARSER_NOTHING_TO_AGAIN
    "Vous ne pouvez pas répéter cela.";
#EndIf;
#Ifndef MSG_PARSER_BE_MORE_SPECIFIC;
Constant MSG_PARSER_BE_MORE_SPECIFIC
    "Vous devez être plus explicite.";
    !précis(e)
#EndIf;
#Ifndef MSG_PARSER_NO_MULTIPLES_FOR_NPC;
Constant MSG_PARSER_NO_MULTIPLES_FOR_NPC
    "Vous ne pouvez vous référer qu'à un seul objet lorsque vous vous adressez à des personnes.";
#EndIf;

#IfDef OPTIONAL_FULL_SCORE;
#IfDef OPTIONAL_SCORED;
#Ifndef MSG_FULLSCORE_OBJECTS;
Constant MSG_FULLSCORE_OBJECTS
    "objets divers trouvés"; !(s)
#EndIf;
#Ifndef MSG_FULLSCORE_ROOMS;
Constant MSG_FULLSCORE_ROOMS
    "lieux visités"; !(s)
#EndIf;
#EndIf;
#Ifndef MSG_FULLSCORE_ACTIONS;
Constant MSG_FULLSCORE_ACTIONS
    "actions remarquables effectuées";
    !(s) importantes
#EndIf;
#EndIf;

!
! complex messages (enumerated)
!

! Note, we can only use id 2-999
Default MSG_CLOSE_YOU_CANT = 2;
Default MSG_ENTER_YOU_CANT = 3;
Default MSG_EXAMINE_NOTHING_SPECIAL = 4;
Default MSG_TAKE_ANIMATE = 5;
Default MSG_TAKE_PLAYER_PARENT = 6;
Default MSG_EAT_ANIMATE = 7;
Default MSG_DROP_NOT_HOLDING = 8;
Default MSG_OPEN_DEFAULT = 9;
Default MSG_CLOSE_DEFAULT = 10;
Default MSG_LOOK_BEFORE_ROOMNAME  = 11;
Default MSG_SHOW_NOT_HOLDING = 12;
Default MSG_SHOW_DEFAULT = 13;
Default MSG_GIVE_NOT_HOLDING = 14;
Default MSG_GIVE_DEFAULT = 15;
Default MSG_ASKFOR_DEFAULT = 16;
Default MSG_ASKTO_DEFAULT = 17;
Default MSG_ENTER_DEFAULT = 18;
Default MSG_EXIT_FIRST_LEAVE = 19;
Default MSG_ENTER_NOT_OPEN = 20;
Default MSG_EXIT_NOT_OPEN = 21;
Default MSG_EXIT_DEFAULT = 22;
Default MSG_INVENTORY_DEFAULT = 23;
Default MSG_GO_FIRST_LEAVE = 24;
Default MSG_GIVE_PLAYER 25;
Default MSG_SAVE_FAILED 26;
Default MSG_RESTORE_FAILED 27;
Default MSG_RESTART_FAILED 28;
Default MSG_INSERT_DEFAULT 29;
Default MSG_INSERT_NOT_OPEN 30;
Default MSG_ASK_DEFAULT 31;
Default MSG_ANSWER_DEFAULT 32;
Default MSG_RESTART_RESTORE_OR_QUIT 33;
Default MSG_AREYOUSUREQUIT 34;
Default MSG_WEAR_ALREADY_WORN 35;
Default MSG_WEAR_NOT_CLOTHING 36;
Default MSG_WEAR_NOT_HOLDING 37;
Default MSG_WEAR_DEFAULT 38;
Default MSG_INSERT_ALREADY 39;
Default MSG_INSERT_NO_ROOM 40;
Default MSG_PUTON_ALREADY 41;
Default MSG_PUTON_NO_ROOM 42;
Default MSG_PUTON_DEFAULT 43;
Default MSG_GO_DOOR_CLOSED 44;
Default MSG_SWITCH_ON_NOT_SWITCHABLE 45;
Default MSG_SWITCH_OFF_NOT_SWITCHABLE 46;
Default MSG_SWITCH_ON_ON 47;
Default MSG_SWITCH_OFF_NOT_ON 48;
Default MSG_SWITCH_ON_DEFAULT 49;
Default MSG_SWITCH_OFF_DEFAULT 50;
Default MSG_PUSH_STATIC 51;
Default MSG_PULL_STATIC 52;
Default MSG_TURN_STATIC 53;
Default MSG_PUSH_SCENERY 54;
Default MSG_PULL_SCENERY 55;
Default MSG_TURN_SCENERY 56;
Default MSG_PUSH_ANIMATE 57;
Default MSG_PULL_ANIMATE 58;
Default MSG_TURN_ANIMATE 59;
Default MSG_TURN_DEFAULT 60;
Default MSG_PUSH_DEFAULT 61;
Default MSG_PULL_DEFAULT 62;
Default MSG_YOU_HAVE_WON 63;
Default MSG_YOU_HAVE_DIED 64;
Default MSG_OPEN_YOU_CANT = 65;
Default MSG_PARSER_NOTHING_TO_VERB 66;
Default MSG_TOUCHABLE_FOUND_CLOSED 67;
Default MSG_CONSULT_NOTHING_INTERESTING 68;
Default MSG_CUT_NO_USE 69;
Default MSG_SACK_PUTTING 70;
Default MSG_LOCK_NOT_A_LOCK 71;
Default MSG_LOCK_ALREADY_LOCKED 72;
Default MSG_LOCK_CLOSE_FIRST 73;
Default MSG_LOCK_KEY_DOESNT_FIT 74;
Default MSG_LOCK_DEFAULT 75;
Default MSG_DISROBE_NOT_WEARING 76;
Default MSG_DISROBE_DEFAULT 77;
Default MSG_REMOVE_CLOSED 78;
Default MSG_REMOVE_NOT_HERE 79;
Default MSG_SEARCH_IN_IT_ISARE 80;
Default MSG_SEARCH_ON_IT_ISARE 81;
Default MSG_SEARCH_EMPTY 82;
Default MSG_SEARCH_NOTHING_ON 83;
Default MSG_SEARCH_CANT_SEE_CLOSED 84;
Default MSG_EAT_DEFAULT = 85;
#Ifdef OPTIONAL_FULL_SCORE;
Default MSG_FULLSCORE_START 86;
Default MSG_FULLSCORE_END 87;
#Endif;
Default MSG_SCORE_DEFAULT 88;
Default MSG_UNLOCK_NOT_A_LOCK 89;
Default MSG_UNLOCK_ALREADY_UNLOCKED 90;
Default MSG_UNLOCK_KEY_DOESNT_FIT 91;
Default MSG_UNLOCK_DEFAULT 92;
Default MSG_ENTER_BAD_LOCATION 93;
Default MSG_PROMPT 94;
#Ifndef OPTIONAL_NO_DARKNESS;
Default MSG_EXAMINE_DARK 95;
Default MSG_SEARCH_DARK 96;
#Endif;
Default MSG_EXAMINE_ONOFF 97;
Default MSG_ORDERS_WONT 98;
Default MSG_AUTO_TAKE 99;
Default MSG_AUTO_DISROBE = 100;
Default MSG_PARSER_PARTIAL_MATCH = 101;
Default MSG_TAKE_BELONGS 102;
Default MSG_TAKE_PART_OF 103;
Default MSG_TAKE_NOT_AVAILABLE 104;
Default MSG_PARSER_CONTAINER_ISNT_OPEN 105;
Default MSG_PARSER_NOT_HOLDING 106;
Default MSG_PARSER_CANT_TALK 107;
Default MSG_WAVE_NOTHOLDING 108;
Default MSG_JUMP_OVER 109;
Default MSG_TIE_DEFAULT 110;
Default MSG_CLOSE_NOT_OPEN 111;
Default MSG_RUB_DEFAULT 112;
Default MSG_SQUEEZE_DEFAULT 113;
Default MSG_EXAMINE_CLOSED 114;
Default MSG_EMPTY_IS_CLOSED 115; ! Only used from extended verbset, but same message also used in basic set.
Default MSG_PARSER_NO_NEED_REFER_TO 116;
Default MSG_PARSER_DONT_UNDERSTAND_WORD 117;
Default MSG_INSERT_NOT_CONTAINER 118;
Default MSG_TRANSFER_ALREADY 119;
Default MSG_YES_OR_NO 120;
Default MSG_RESTART_CONFIRM 121;
#Ifndef NO_SCORE;
Default MSG_PARSER_NEW_SCORE 122;
#Endif;
Default MSG_CLIMB_ANIMATE 123;
Default MSG_CLIMB_DEFAULT 124;
Default MSG_PARSER_BAD_PATTERN_PREFIX 125;
Default MSG_PARSER_BAD_PATTERN_SUFFIX 126;
Default MSG_TAKE_ALREADY_HAVE 127;
Default MSG_SHOUT_DEFAULT 128;
Default MSG_SHOUTAT_DEFAULT 129;
Default MSG_INSERT_ANIMATE 130;
Default MSG_PUTON_ANIMATE 131;
Default MSG_LOOKMODE_NORMAL 132;
Default MSG_LOOKMODE_LONG 133;
Default MSG_LOOKMODE_SHORT 134;
Default MSG_AUTO_TAKE_NOT_HELD = 135;
Default MSG_AUTO_DISROBE_WORN = 136;
Default MSG_TAKE_SCENERY = 137;
Default MSG_TAKE_STATIC = 138;
Default MSG_EAT_INEDIBLE = 139;
Default MSG_OPEN_ALREADY = 140;
Default MSG_OPEN_LOCKED = 141;
Default MSG_PUTON_NOT_SUPPORTER = 142;
Default MSG_PARSER_NO_IT = 143;
Default MSG_PARSER_CANT_SEE_IT = 144;
Default MSG_NOTIFY_ON = 145;
Default MSG_NOTIFY_OFF = 146;
Default MSG_ENTER_HELD 147;

#IfDef OPTIONAL_PROVIDE_UNDO_FINAL;
#Ifndef MSG_UNDO_NOTHING_DONE;
Constant MSG_UNDO_NOTHING_DONE
    "[Il n'y a aucune action à annuler.]";
#EndIf;
#Ifndef MSG_UNDO_NOT_PROVIDED;
Constant MSG_UNDO_NOT_PROVIDED
    "[Votre interpréteur ne permet pas d'annuler.]";
#EndIf;
#Ifndef MSG_UNDO_FAILED;
Constant MSG_UNDO_FAILED
    "La commande « annuler » a échoué.";
#EndIf;
#Ifndef MSG_UNDO_DONE;
Constant MSG_UNDO_DONE
    "L'action précédente a été annulée.";
    !La commande
#EndIf;
#EndIf;


#IfDef OPTIONAL_EXTENDED_VERBSET;
#Ifndef MSG_BURN_DEFAULT;
Constant MSG_BURN_DEFAULT
    "Cela n'apporterait rien.";
    !ne mènerait à
#EndIf;
#Ifndef MSG_BUY_DEFAULT;
Constant MSG_BUY_DEFAULT
    "Rien n'est en vente.";
    ! en/à ne semble
#EndIf;
#Ifndef MSG_EMPTY_WOULDNT_ACHIEVE;
Constant MSG_EMPTY_WOULDNT_ACHIEVE
    "Cela ne viderait rien du tout.";
    !du tout?
#EndIf;
#Ifndef MSG_RHETORICAL_QUESTION;
Constant MSG_RHETORICAL_QUESTION
    "Ni oui ni non.";
    !That was a rhetorical question.
#EndIf;
#Ifndef MSG_PRAY_DEFAULT;
Constant MSG_PRAY_DEFAULT
    "Votre prière semble sans effet.";
#EndIf;
#Ifndef MSG_SING_DEFAULT;
Constant MSG_SING_DEFAULT
    "Vous fredonnez un air qui vous est familier.";
    !qui vous est
#EndIf;
#Ifndef MSG_SLEEP_DEFAULT;
Constant MSG_SLEEP_DEFAULT
    "Vous ne vous sentez pas particulièrement fatigué(e).";
#EndIf;
#Ifndef MSG_SORRY_DEFAULT;
Constant MSG_SORRY_DEFAULT
    "N'en faites pas trop.";
#EndIf;
#Ifndef MSG_SQUEEZE_YOURSELF;
Constant MSG_SQUEEZE_YOURSELF
    "Tenez-vous tranquille.";
    !un peu
#EndIf;

#Ifndef MSG_SWIM_DEFAULT;
Constant MSG_SWIM_DEFAULT
    "Il n'y a pas vraiment d'endroit où nager.";
    !d'endroit approprié
#EndIf;
#Ifndef MSG_SWING_DEFAULT;
Constant MSG_SWING_DEFAULT
    "Il n'y a rien de vraiment approprié.";
#EndIf;
#Ifndef MSG_TASTE_DEFAULT;
Constant MSG_TASTE_DEFAULT
    "Vous ne percevez rien de particulier.";
    !déceler/éprouvez/rien d'inattendu
#EndIf;
#Ifndef MSG_THINK_DEFAULT;
Constant MSG_THINK_DEFAULT
    "Travaillez donc à bien penser.";
    !Quelle bonne idée !
#EndIf;
#Ifndef MSG_WAVEHANDS_DEFAULT;
Constant MSG_WAVEHANDS_DEFAULT
    "Vous saluez d'un signe de la main.";
#EndIf;
#Ifndef MSG_WAKE_DEFAULT;
Constant MSG_WAKE_DEFAULT
    "Ce n'est pourtant pas un rêve.";
#Endif;
#Ifndef MSG_WAKEOTHER_DEFAULT;
Constant MSG_WAKEOTHER_DEFAULT
    "Cela ne semble pas nécessaire.";
#Endif;
#Ifndef MSG_KISS_PLAYER;
Constant MSG_KISS_PLAYER
    "Si vous pensez que cela peut aider.";
    !ça
#Endif;
#Ifndef MSG_KISS_DEFAULT;
Constant MSG_KISS_DEFAULT
    "Concentrez-vous plutôt sur le jeu.";
    !Restez plutôt/de préférence
#Endif;
#Ifndef MSG_MILD_DEFAULT;
Constant MSG_MILD_DEFAULT
    "Tout à fait.";
    !Assez
#EndIf;
#Ifndef MSG_STRONG_DEFAULT;
Constant MSG_STRONG_DEFAULT
    "Les vrais aventuriers n'utilisent pas un tel langage.";
#EndIf;

Default MSG_BLOW_DEFAULT 200;
Default MSG_WAVE_DEFAULT 201;
Default MSG_EMPTY_ALREADY_EMPTY 202;
Default MSG_SET_DEFAULT 203;
Default MSG_SET_TO_DEFAULT 204;
Default MSG_EMPTY_NOT_CONTAINER 205;
#EndIf;

#Iffalse MSG_PUSH_DEFAULT < 1000;
#Iffalse MSG_PULL_DEFAULT < 1000;
#Iffalse MSG_TURN_DEFAULT < 1000;
Constant SKIP_MSG_PUSH_DEFAULT;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_PUSH_STATIC < 1000;
#Iffalse MSG_PULL_STATIC < 1000;
#Iffalse MSG_TURN_STATIC < 1000;
#Iffalse MSG_TAKE_STATIC < 1000;
Constant SKIP_MSG_PUSH_STATIC;
#Endif;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_PUSH_SCENERY < 1000;
#Iffalse MSG_PULL_SCENERY < 1000;
#Iffalse MSG_TURN_SCENERY < 1000;
Constant SKIP_MSG_PUSH_SCENERY;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_PUSH_ANIMATE < 1000;
#Iffalse MSG_PULL_ANIMATE < 1000;
#Iffalse MSG_TURN_ANIMATE < 1000;
#Iffalse MSG_CLIMB_ANIMATE < 1000;
Constant SKIP_MSG_PUSH_ANIMATE;
#Endif;
#Endif;
#Endif;
#Endif;


#Iffalse MSG_DROP_NOT_HOLDING < 1000;
#Iffalse MSG_SHOW_NOT_HOLDING < 1000;
#Iffalse MSG_GIVE_NOT_HOLDING < 1000;
#Iffalse MSG_WEAR_NOT_HOLDING < 1000;
Constant SKIP_MSG_DROP_NOT_HOLDING;
#Endif;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_OPEN_YOU_CANT < 1000;
#Iffalse MSG_CLOSE_YOU_CANT < 1000;
#Iffalse MSG_ENTER_YOU_CANT < 1000;
#Iffalse MSG_LOCK_NOT_A_LOCK < 1000;
#Iffalse MSG_UNLOCK_NOT_A_LOCK < 1000;
#Iffalse MSG_WEAR_NOT_CLOTHING < 1000;
Constant SKIP_MSG_OPEN_YOU_CANT;
#Endif;
#Endif;
#Endif;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_TAKE_ANIMATE < 1000;
#Iffalse MSG_EAT_ANIMATE < 1000;
Constant SKIP_MSG_TAKE_ANIMATE;
#Endif;
#Endif;

#Iffalse MSG_TAKE_PLAYER_PARENT < 1000;
#Iffalse MSG_GO_FIRST_LEAVE < 1000;
#Iffalse MSG_EXIT_FIRST_LEAVE < 1000;
Constant SKIP_MSG_TAKE_PLAYER_PARENT;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_CLOSE_DEFAULT < 1000;
#Iffalse MSG_ENTER_DEFAULT < 1000;
#Iffalse MSG_LOCK_DEFAULT < 1000;
#Iffalse MSG_UNLOCK_DEFAULT < 1000;
#Iffalse MSG_EXIT_DEFAULT < 1000;
Constant SKIP_MSG_CLOSE_DEFAULT;
#Endif;
#Endif;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_GIVE_DEFAULT < 1000;
#Iffalse MSG_SHOW_DEFAULT < 1000;
Constant SKIP_MSG_GIVE_DEFAULT;
#Endif;
#Endif;

#Iffalse MSG_ASKFOR_DEFAULT < 1000;
#Iffalse MSG_ASKTO_DEFAULT < 1000;
#Iffalse MSG_ORDERS_WONT < 1000;
Constant SKIP_MSG_ASKFOR_DEFAULT;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_ENTER_NOT_OPEN < 1000;
#Iffalse MSG_EXIT_NOT_OPEN < 1000;
#Iffalse MSG_INSERT_NOT_OPEN < 1000;
#Iffalse MSG_GO_DOOR_CLOSED < 1000;
#Iffalse MSG_EMPTY_IS_CLOSED < 1000;
#Iffalse MSG_REMOVE_CLOSED < 1000;
Constant SKIP_MSG_ENTER_NOT_OPEN;
#Endif;
#Endif;
#Endif;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_GIVE_PLAYER < 1000;
#Iffalse MSG_TAKE_ALREADY_HAVE < 1000;
Constant SKIP_MSG_GIVE_PLAYER;
#Endif;
#Endif;

#Iffalse MSG_SAVE_FAILED < 1000;
#Iffalse MSG_RESTORE_FAILED < 1000;
#Iffalse MSG_RESTART_FAILED < 1000;
Constant SKIP_MSG_SAVE_FAILED;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_INSERT_ALREADY < 1000;
#Iffalse MSG_PUTON_ALREADY < 1000;
#Iffalse MSG_TRANSFER_ALREADY < 1000;
Constant SKIP_MSG_INSERT_ALREADY;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_INSERT_ANIMATE < 1000;
#Iffalse MSG_PUTON_ANIMATE < 1000;
Constant SKIP_MSG_INSERT_ANIMATE;
#Endif;
#Endif;

#Iffalse MSG_INSERT_NO_ROOM < 1000;
#Iffalse MSG_PUTON_NO_ROOM < 1000;
Constant SKIP_MSG_INSERT_NO_ROOM;
#Endif;
#Endif;

#Iffalse MSG_ASK_DEFAULT < 1000;
#Iffalse MSG_ANSWER_DEFAULT < 1000;
#Iffalse MSG_SHOUT_DEFAULT < 1000;
#Iffalse MSG_SHOUTAT_DEFAULT < 1000;
Constant SKIP_MSG_ASK_DEFAULT;
#Endif;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_SWITCH_ON_NOT_SWITCHABLE < 1000;
#Iffalse MSG_SWITCH_OFF_NOT_SWITCHABLE < 1000;
Constant SKIP_MSG_SWITCH_ON_NOT_SWITCHABL;
#Endif;
#Endif;

#Iffalse MSG_SWITCH_ON_ON < 1000;
#Iffalse MSG_SWITCH_OFF_NOT_ON < 1000;
Constant SKIP_MSG_SWITCH_ON_ON;
#Endif;
#Endif;

#Iffalse MSG_SWITCH_ON_DEFAULT < 1000;
#Iffalse MSG_SWITCH_OFF_DEFAULT < 1000;
Constant SKIP_MSG_SWITCH_ON_DEFAULT;
#Endif;
#Endif;

#Iffalse MSG_PARSER_NOT_HOLDING < 1000;
#Iffalse MSG_AUTO_TAKE_NOT_HELD < 1000;
#Iffalse MSG_WAVE_NOTHOLDING < 1000;
Constant SKIP_MSG_PARSER_NOT_HOLDING;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_CLOSE_NOT_OPEN < 1000;
#Iffalse MSG_TOUCHABLE_FOUND_CLOSED < 1000;
#Iffalse MSG_PARSER_CONTAINER_ISNT_OPEN < 1000;
Constant SKIP_MSG_CLOSE_NOT_OPEN;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_CUT_NO_USE < 1000;
#Iffalse MSG_JUMP_OVER < 1000;
#Iffalse MSG_TIE_DEFAULT < 1000;
#Iffalse MSG_CLIMB_DEFAULT < 1000;
Constant SKIP_MSG_CUT_NO_USE;
#Endif;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_LOCK_ALREADY_LOCKED < 1000;
#Iffalse MSG_UNLOCK_ALREADY_UNLOCKED < 1000;
Constant SKIP_MSG_LOCK_ALREADY_LOCKED;
#Endif;
#Endif;

#Iffalse MSG_LOCK_KEY_DOESNT_FIT < 1000;
#Iffalse MSG_UNLOCK_KEY_DOESNT_FIT < 1000;
Constant SKIP_MSG_LOCK_KEY_DOESNT_FIT;
#Endif;
#Endif;

#Iffalse MSG_RUB_DEFAULT < 1000;
#Iffalse MSG_SQUEEZE_DEFAULT < 1000;
Constant SKIP_MSG_RUB_DEFAULT;
#Endif;
#Endif;

#Iffalse MSG_LOOKMODE_NORMAL < 1000;
#Iffalse MSG_LOOKMODE_LONG < 1000;
#Iffalse MSG_LOOKMODE_SHORT < 1000;
Constant SKIP_MSG_LOOKMODE;
#Endif;
#Endif;
#Endif;

#Iffalse MSG_INSERT_NOT_CONTAINER < 1000;
#Ifndef MSG_EMPTY_NOT_CONTAINER;
Constant SKIP_MSG_INSERT_NOT_CONTAINER;
#Ifnot;
#Iffalse MSG_EMPTY_NOT_CONTAINER < 1000;
Constant SKIP_MSG_INSERT_NOT_CONTAINER;
#Endif;
#Endif;
#Endif;

#Ifndef OPTIONAL_NO_SCORE;
#Iffalse MSG_NOTIFY_ON < 1000;
#Iffalse MSG_NOTIFY_OFF < 1000;
Constant SKIP_MSG_NOTIFY_ON;
#Endif;
#Endif;
#Endif;

#Ifndef OPTIONAL_NO_DARKNESS;
#Iffalse MSG_EXAMINE_DARK < 1000;
#Iffalse MSG_SEARCH_DARK < 1000;
Constant SKIP_MSG_EXAMINE_DARK;
#Endif;
#Endif;
#Endif;

[ _PrintMsg p_msg p_arg_1 p_arg_2;
    if(p_msg ofclass String)
        print_ret (string) p_msg;

#Ifdef LibraryMessages;
    if(p_msg > 999) {
        return LibraryMessages(p_msg, p_arg_1, p_arg_2);
    }
#Endif;

    ! Not a string, there should be code for the message here
    switch(p_msg) {
#Iftrue MSG_TAKE_SCENERY < 1000;
    MSG_TAKE_SCENERY:
        print_ret (CTheyreorThats) noun, "difficilement transportable", (_s) noun, ".";
#EndIf;
#Ifndef SKIP_MSG_PUSH_DEFAULT;
    MSG_PUSH_DEFAULT, MSG_PULL_DEFAULT, MSG_TURN_DEFAULT:
        "Il ne se produit rien de particulier.";
        !se passe
        !Vous n'aboutirez à rien comme cela.
#Endif;
#Ifndef SKIP_MSG_PUSH_STATIC;
    MSG_PUSH_STATIC, MSG_PULL_STATIC, MSG_TURN_STATIC, MSG_TAKE_STATIC:
        print_ret (CTheyreorThats) noun, "fixé", (_es) noun, " sur place.";
#Endif;
#Ifndef SKIP_MSG_PUSH_SCENERY;
    MSG_PUSH_SCENERY, MSG_PULL_SCENERY, MSG_TURN_SCENERY:
        "Ce n'est pas quelque chose que vous pouvez ", (verbName) verb_word, ".";
        !"Quelle idée !";
#Endif;
#IfDef SACK_OBJECT;
#IfTrue MSG_SACK_PUTTING < 1000;
    MSG_SACK_PUTTING:
    ! p_arg_1 = the object being put into SACK_OBJECT.
        "(mettant ", (the) p_arg_1, " dans ", (the) SACK_OBJECT, " pour faire de la place)";
#EndIf;
#EndIf;
#IfTrue MSG_PROMPT < 1000;
    MSG_PROMPT:
    #ifdef CHAR_PROMPT;
        @print_char '>';
    #ifnot;
        print "> ";
    #endif;
        rtrue;
#EndIf;
#IfTrue MSG_INVENTORY_DEFAULT < 1000;
    MSG_INVENTORY_DEFAULT:
        ! return true if something listed to run afterroutines
        ! or false if MSG_INVENTORY_EMPTY should be displayed
        p_arg_1 = "Vous avez ";  !transportez (avec vous)
        if(inventory_style == 0) {
            p_arg_1 = "Vous avez :";
            p_arg_2 = NEWLINE_BIT;
        }
        if(PrintContents(p_arg_1, player, p_arg_2)) {
            if(inventory_style) print ".^";
            rtrue;
        }
        rfalse;
#EndIf;
#IfTrue MSG_EXAMINE_NOTHING_SPECIAL < 1000;
    MSG_EXAMINE_NOTHING_SPECIAL:
        "Vous ne remarquez rien de particulier concernant ", (the) noun, ".";
#EndIf;
#Ifndef SKIP_MSG_PUSH_ANIMATE;
    MSG_PUSH_ANIMATE, MSG_PULL_ANIMATE, MSG_TURN_ANIMATE, MSG_CLIMB_ANIMATE:
        "Ce ne serait pas très correct.";
#Endif;
#Ifndef SKIP_MSG_DROP_NOT_HOLDING;
    MSG_DROP_NOT_HOLDING, MSG_SHOW_NOT_HOLDING, MSG_GIVE_NOT_HOLDING,
    MSG_WEAR_NOT_HOLDING:
        "Vous ne", (_lesl) noun, "avez pas avec vous.";
        !transportez
#Endif;
#Ifndef SKIP_MSG_OPEN_YOU_CANT; !TODO tester
    MSG_OPEN_YOU_CANT, MSG_CLOSE_YOU_CANT, MSG_ENTER_YOU_CANT,
    MSG_LOCK_NOT_A_LOCK, MSG_UNLOCK_NOT_A_LOCK, MSG_WEAR_NOT_CLOTHING:
        !"You can't ", (verbname) p_arg_1, " ", (ThatorThose) noun, ".";
        print "Ce n'est pas quelque chose ";
        if (p_msg == MSG_ENTER_YOU_CANT) "où vous pouvez aller.";
        else print "que vous pouvez ", (verbname) p_arg_1;
        if (p_msg == MSG_WEAR_NOT_CLOTHING) print " sur vous";
        ".";
#Endif;
#IfTrue MSG_EAT_INEDIBLE < 1000;
    MSG_EAT_INEDIBLE:
        print_ret (CTheyreorThatsNeg) noun, "comestible", (_s) noun, ".";
        !vraiement
#EndIf;
#IfTrue MSG_OPEN_ALREADY < 1000;
    MSG_OPEN_ALREADY:
        print_ret (CObjIs) noun, "déjà ouvert", (_es) noun, ".";
#EndIf;
#IfTrue MSG_OPEN_LOCKED < 1000;
    MSG_OPEN_LOCKED:
        print_ret (CObjIs) noun, "verrouillé", (_es) noun, ".";
#EndIf;
#IfTrue MSG_PUTON_NOT_SUPPORTER < 1000;
    MSG_PUTON_NOT_SUPPORTER:
        "Vous ne pouvez rien mettre sur ", (the) second, ".";
        !rien poser déposer
#EndIf;
#Ifndef SKIP_MSG_TAKE_ANIMATE;
    MSG_TAKE_ANIMATE, MSG_EAT_ANIMATE:
        print "Je doute que ", (the) noun, " soi";
        if (noun has pluralname) print "en";
        "t d'accord avec cela."; !ça
#Endif;
#Ifndef SKIP_MSG_TAKE_PLAYER_PARENT;
    MSG_TAKE_PLAYER_PARENT, MSG_GO_FIRST_LEAVE, MSG_EXIT_FIRST_LEAVE:
        print "Vous devez d'abord ";
        if (p_arg_1 has supporter) print "descendre";
        else print "sortir";
        print_ret (_dedudes) p_arg_1, (name) p_arg_1, ".";
#Endif;
#Iftrue MSG_OPEN_DEFAULT < 1000; !FIXME négation
    MSG_OPEN_DEFAULT:
        print "Vous ouvrez ", (the) noun;
        if(noun has container && noun hasnt transparent &&
            ~~IndirectlyContains(noun, player)) {
            @print_char ',';
            p_arg_1 = child(noun); !WARNING redéfinition de p_arg_1
            while (p_arg_1) {
                if (p_arg_1 hasnt concealed && p_arg_1 hasnt scenery)
                    jump no_negation;
                p_arg_1 = sibling(p_arg_1);
            }
            print " ne";
            .no_negation;
            print " révélant ";
            if(PrintContents(0, noun)==false) print "rien du tout";
            !du tout ?
        }
        ".";
#Endif;
#Iftrue MSG_LOOK_BEFORE_ROOMNAME < 1000;
    MSG_LOOK_BEFORE_ROOMNAME:
        ! what to write at first when describing a room. Can be used to
        ! add a newline, but default is to write nothing.
        !@new_line;
#Endif;
#Ifndef SKIP_MSG_CLOSE_DEFAULT;
    !p_arg_1 = the base verb for this action ('open', 'close' etc).
    MSG_CLOSE_DEFAULT:
        "Vous fermez ", (the) noun, ".";
    MSG_LOCK_DEFAULT:
        "Vous verrouillez ", (the) noun, ".";
    MSG_UNLOCK_DEFAULT:
        "Vous déverrouillez ", (the) noun, ".";
    MSG_ENTER_DEFAULT:
        print "Vous allez ";
        if (noun has container) print "dans";
        else print "sur";
        " ", (the) noun, ".";
    MSG_EXIT_DEFAULT:
        print "Vous ";
        if (noun has container) print "sortez";
        else print "descendez";
        print_ret (_dedudes) noun, (name) noun, ".";
#Endif;
#Ifndef SKIP_MSG_GIVE_DEFAULT;
    MSG_GIVE_DEFAULT, MSG_SHOW_DEFAULT:
        print_ret (The) second, " ne semble", (_nt) second, "pas intéressé", (_es) second, ".";
#Endif;
#Ifndef SKIP_MSG_ASKFOR_DEFAULT;
    MSG_ASKFOR_DEFAULT, MSG_ASKTO_DEFAULT, MSG_ORDERS_WONT:
    ! p_arg_1 = the actor which the player has asked to do something.
        print (The) p_arg_1;
        if (noun has pluralname) print " ont";
        else print " a";
        " mieux à faire."; !probablement
#Endif;
#Ifndef SKIP_MSG_ENTER_NOT_OPEN;
    MSG_ENTER_NOT_OPEN, MSG_EXIT_NOT_OPEN, MSG_INSERT_NOT_OPEN,
    MSG_GO_DOOR_CLOSED, MSG_EMPTY_IS_CLOSED, MSG_REMOVE_CLOSED:
    ! p_arg_1 = the object which is closed, thus blocking the player's action.
        "C'est impossible, car ", (the) p_arg_1, " ", (IsorAre) p_arg_1, "fermé", (_es) p_arg_1, ".";
        !irréalisable/infaisable
#Endif;
#Ifndef SKIP_MSG_GIVE_PLAYER;
    MSG_GIVE_PLAYER, MSG_TAKE_ALREADY_HAVE:
        "Vous", (_lesl) noun, "avez déjà avec vous.";
#Endif;
#Ifndef SKIP_MSG_SAVE_FAILED;
    MSG_SAVE_FAILED, MSG_RESTORE_FAILED, MSG_RESTART_FAILED:
        "La commande « ", (verbname) verb_word, " » a échoué.";
        !"La commande a échoué.";
#Endif;
#Ifndef SKIP_MSG_INSERT_ALREADY;
    MSG_INSERT_ALREADY, MSG_PUTON_ALREADY, MSG_TRANSFER_ALREADY:
        print_ret (CThatorThose) noun, " y ", (IsorAre) noun, "déjà.";
        !"Already there.";
#Endif;
#Ifndef SKIP_MSG_INSERT_ANIMATE;
    MSG_INSERT_ANIMATE, MSG_PUTON_ANIMATE:
        "Essayez plutôt de", (ThatorThose) noun, "donner.";
        !print "Essayez plutôt de", (ThatorThose) noun;
        !if (second has pluralname) print "leur ";
        !else print "lui ";
        !"donner.";
#Endif;
#Ifndef SKIP_MSG_INSERT_NO_ROOM;
    MSG_INSERT_NO_ROOM, MSG_PUTON_NO_ROOM:
        "Il n'y a plus suffisamment de place."; !suffisamment ?
#Endif;
#IfTrue MSG_INSERT_DEFAULT < 1000;
    MSG_INSERT_DEFAULT:
        "Vous mettez ", (the) noun, " dans ", (the) second, ".";
        !insérez
#EndIf;
#IfTrue MSG_PUTON_DEFAULT < 1000;
    MSG_PUTON_DEFAULT:
        "Vous mettez ", (the) noun, " sur ", (the) second, ".";
        !posez
#EndIf;
#Ifndef SKIP_MSG_ASK_DEFAULT;
    MSG_ASK_DEFAULT, MSG_ANSWER_DEFAULT, MSG_SHOUT_DEFAULT, MSG_SHOUTAT_DEFAULT:
        "Vous n'obtenez aucune réponse.";
#Endif;
#IfTrue MSG_WEAR_ALREADY_WORN < 1000;
    MSG_WEAR_ALREADY_WORN:
        "Vous", (_lesl) noun, "avez déjà sur vous.";
#EndIf;
#IfTrue MSG_WEAR_DEFAULT < 1000;
    MSG_WEAR_DEFAULT:
        "Vous avez maintenant ", (the) noun, " sur vous.";
        !portez
        !"Vous", (_lesl) noun, "avez maintenant sur vous.";
#EndIf;
#IfTrue MSG_DISROBE_NOT_WEARING < 1000;
    MSG_DISROBE_NOT_WEARING:
        "Vous n'avez pas ", (the) noun, " sur vous.";
        !"Vous ne", (_lesl) noun, "avez pas sur vous.";
        !portez
#EndIf;
#IfTrue MSG_DISROBE_DEFAULT < 1000;
    MSG_DISROBE_DEFAULT:
        "Vous retirez ", (the) noun, "."; !enlevez
#EndIf;
#Ifndef SKIP_MSG_SWITCH_ON_NOT_SWITCHABL;
    MSG_SWITCH_ON_NOT_SWITCHABLE, MSG_SWITCH_OFF_NOT_SWITCHABLE:
        "Ce n'est pas quelque chose que vous pouvez ", (verbName) verb_word, ".";
        !démarrer/arrêter, activer/désactiver, etc.
#Endif;
#Ifndef SKIP_MSG_SWITCH_ON_ON;
    MSG_SWITCH_ON_ON, MSG_SWITCH_OFF_NOT_ON:
        print_ret (CTheyreorThats) noun, "déjà ", (OnOff) noun, ".";
#Endif;
#Ifndef SKIP_MSG_SWITCH_ON_DEFAULT;
    MSG_SWITCH_ON_DEFAULT:
        "Vous allumez ", (the) noun, ".";
        !activez éclairez/OK
    MSG_SWITCH_OFF_DEFAULT:
        "Vous éteignez ", (the) noun, ".";
        !désactivez/OK
#Endif;
#Iftrue MSG_AUTO_TAKE < 1000;
    MSG_AUTO_TAKE:
    ! p_arg_1 = the object the player automatically picks up
    print "(prenant d'abord ", (the) p_arg_1, ")^";
#Endif;
#Iftrue MSG_AUTO_DISROBE < 1000;
    MSG_AUTO_DISROBE:
    ! p_arg_1 = the object the player automatically takes off.
        print "(retirant d'abord ", (the) p_arg_1, ")^";
#Endif;
#Iftrue MSG_AUTO_DISROBE_WORN < 1000;
    MSG_AUTO_DISROBE_WORN:
    ! p_arg_1 = the object the player would need to take off.
        print "Vous devez d'abord retirer ", (the) p_arg_1, ".^";
        !print "Vous devez d'abord ", (ThatorThose), "retirer.^";
#Endif;
#IfTrue MSG_PARSER_NOTHING_TO_VERB < 1000;
    MSG_PARSER_NOTHING_TO_VERB:
    ! p_arg_1 = the last word in player input + 1.
        if(action == ##Drop or ##Insert) {
            if((parse + 2 + (p_arg_1 - 2) *4) --> 0 == ALL_WORD)
                "Vous n'avez rien avec vous."; !transportez
            if(TryNumber(verb_wordnum + 1) > 0) "Vous n'en avez pas avec vous.";
        }
        print "Il n'y a rien qui corresponde à « "; !TODO
        _PrintPartialMatch(verb_wordnum, p_arg_1 - 1);
        " ».";
#EndIf;
#Ifndef SKIP_MSG_PARSER_NOT_HOLDING;
    MSG_PARSER_NOT_HOLDING, MSG_AUTO_TAKE_NOT_HELD, MSG_WAVE_NOTHOLDING:
    ! p_arg_1 = the object which the player must be holding to perform the
    ! action but isn't.
        print_ret "Vous n'avez pas ", (the) p_arg_1, " avec vous.";
        !print_ret "Vous ne", (_lesl) p_arg_1, "avez pas avec vous.";
#Endif;
#IfTrue MSG_PARSER_PARTIAL_MATCH < 1000;
    MSG_PARSER_PARTIAL_MATCH:
    ! p_arg_1 = the word number of the last word understood + 1.
        print "Je n'ai pris en compte que « ";
        _PrintPartialMatch(verb_wordnum, p_arg_1);
        " ».";
#EndIf;
#IfTrue MSG_PARSER_CANT_TALK < 1000;
    MSG_PARSER_CANT_TALK:
    ! p_arg_1 = the object which can't be talked to.
        print_ret "Vous ne pouvez pas parler", (_aaux) p_arg_1, (name) p_arg_1, ".";
#EndIf;
#IfTrue MSG_PARSER_NO_NEED_REFER_TO < 1000; !TODO
    MSG_PARSER_NO_NEED_REFER_TO:
        print "Vous n'avez pas besoin de vous référer à « ";
        _PrintUnknownWord();
        print_ret " » dans ce jeu.";
#EndIf;
#IfTrue MSG_PARSER_DONT_UNDERSTAND_WORD < 1000;
    MSG_PARSER_DONT_UNDERSTAND_WORD:
        print "Je ne comprends pas ce que « "; !désolé ?
        _PrintUnknownWord();
        print_ret " » veut dire.";
#EndIf;
#IfTrue MSG_PARSER_BAD_PATTERN_PREFIX < 1000;
    MSG_PARSER_BAD_PATTERN_PREFIX:
        print "Je pense que vous vouliez « ";
        !souhaitiez
        !print "Si vous vouliez « ";
        rtrue;
#EndIf;
#IfTrue MSG_PARSER_BAD_PATTERN_SUFFIX < 1000; !TODO
    MSG_PARSER_BAD_PATTERN_SUFFIX:
        " ». Veuillez reformuler.";
#EndIf;
#IfTrue MSG_PARSER_NO_IT < 1000;
    MSG_PARSER_NO_IT:
        "Je ne sais pas à quoi « ",(LanguagePronom) p_arg_1, " » se réfère.";
#EndIf;
#IfTrue MSG_PARSER_CANT_SEE_IT < 1000; !TODO
    MSG_PARSER_CANT_SEE_IT:
        "Vous ne voyez pas « ",(LanguagePronom) p_arg_1, " » (", (name) p_arg_2, ") pour l'instant.";
#EndIf;
#Ifndef SKIP_MSG_CLOSE_NOT_OPEN;
    MSG_CLOSE_NOT_OPEN:
        print_ret (CObjIs) p_arg_1, "déjà fermé", (_es) p_arg_1, ".";
    MSG_TOUCHABLE_FOUND_CLOSED, MSG_PARSER_CONTAINER_ISNT_OPEN:
    ! p_arg_1 = the object which isn't open.
        print_ret (CObjIsNeg) p_arg_1, "ouvert", (_es) p_arg_1, ".";
#Endif;
#IfTrue MSG_CONSULT_NOTHING_INTERESTING < 1000;
    MSG_CONSULT_NOTHING_INTERESTING:
        "Vous ne trouvez rien d'intéressant dans ", (the) noun, ".";
#EndIf;
#Ifndef SKIP_MSG_CUT_NO_USE; !TODO
    MSG_CUT_NO_USE, MSG_TIE_DEFAULT:
        "Vous n'aboutirez à rien comme cela.";
        !n'arriverez
    MSG_CLIMB_DEFAULT, MSG_JUMP_OVER:
        "Ce n'est pas quelque chose où vous pouvez aller.";
#Endif;
#Ifndef SKIP_MSG_LOCK_ALREADY_LOCKED;
    MSG_LOCK_ALREADY_LOCKED:
        print_ret (CObjIs) noun, "déjà verrouillé", (_es) noun, ".";
    MSG_UNLOCK_ALREADY_UNLOCKED:
        print_ret (CObjIs) noun, "déjà déverrouillé", (_es) noun, ".";
#Endif;
#IfTrue MSG_LOCK_CLOSE_FIRST < 1000;
    MSG_LOCK_CLOSE_FIRST:
        "Vous devez d'abord fermer ", (the) noun, ".";
#EndIf;
#Ifndef SKIP_MSG_LOCK_KEY_DOESNT_FIT; !TODO
    MSG_LOCK_KEY_DOESNT_FIT, MSG_UNLOCK_KEY_DOESNT_FIT:
        !print_ret (The) second, " ne semble", (_nt) second, "pas correspondre à la serrure.";
        !print_ret (The) second, " ne semble", (_nt) second, "pas adapté", (_es) second, ".";
        print_ret (The) second, " ne semble", (_nt) second, "pas convenir.";
#Endif;
#IfTrue MSG_EXAMINE_CLOSED < 1000;
    MSG_EXAMINE_CLOSED:
    ! p_arg_1 = the examines object (which is closed).
        print_ret (CObjIs) p_arg_1, "fermé", (_es) p_arg_1, ".";
#Endif;
#IfTrue MSG_REMOVE_NOT_HERE < 1000;
    MSG_REMOVE_NOT_HERE:
        print_ret (CObjIsNeg) noun, "pas ici.";
#EndIf;
#IfTrue MSG_SEARCH_IN_IT_ISARE < 1000;
    MSG_SEARCH_IN_IT_ISARE:
        print (The) noun, " contien";
        if (noun has pluralname) print "nen";
        print "t ";
        PrintContents(0, noun);
        ".";
#EndIf;
#IfTrue MSG_SEARCH_ON_IT_ISARE < 1000;
    MSG_SEARCH_ON_IT_ISARE:
        print "Sur ", (the) noun, ", vous voyez ";
        !il y a
        PrintContents(0, noun);
        ".";
#EndIf;
#IfTrue MSG_SEARCH_EMPTY < 1000;
    MSG_SEARCH_EMPTY:
        print_ret (CObjIs) noun, "vide", (_s) noun, ".";
#EndIf;
#IfTrue MSG_SEARCH_NOTHING_ON < 1000;
    MSG_SEARCH_NOTHING_ON:
        "Il n'y a rien sur ", (the) noun, ".";
        !Vous ne voyez
#EndIf;
#IfTrue MSG_SEARCH_CANT_SEE_CLOSED < 1000;
    MSG_SEARCH_CANT_SEE_CLOSED:
        "C'est impossible, car ", (the) noun, " ", (IsorAre) noun, "fermé", (_es) noun, ".";
        !"You can't see inside, since ", (the) noun, " ", (IsorAre) noun, " closed.";
#EndIf;
#IfTrue MSG_EXAMINE_ONOFF < 1000;
    MSG_EXAMINE_ONOFF:
        print_ret (CObjIs) noun, "actuellement ", (onoff) noun, ".";
#EndIf;
#IfTrue MSG_EAT_DEFAULT < 1000;
    MSG_EAT_DEFAULT:
       "Vous mangez ", (the) noun, ". Pas mauvais.";
#EndIf;
#Ifndef SKIP_MSG_RUB_DEFAULT;
    MSG_RUB_DEFAULT, MSG_SQUEEZE_DEFAULT:
        "Il ne se produit rien de particulier.";
        !"Vous n'aboutirez à rien comme cela.";
#Endif;
#IfTrue MSG_TAKE_NOT_AVAILABLE < 1000;
    MSG_TAKE_NOT_AVAILABLE:
        print (CObjIsNeg) noun; "accessible", (_s) noun, ".";
#EndIf;
#IfTrue MSG_TAKE_BELONGS < 1000;
    MSG_TAKE_BELONGS:
        ! p_arg_1 = the object that is held by p_arg_2
        print_ret (The) p_arg_1, " semble", (_nt) p_arg_1, "appartenir", (_aaux) p_arg_2,
        (name) p_arg_2, ".";
#EndIf;
#IfTrue MSG_TAKE_PART_OF < 1000;
    MSG_TAKE_PART_OF:
        ! p_arg_1 = the object that is part of p_arg_2
        print_ret (The) p_arg_1, " semble", (_nt) p_arg_1, "faire partie", (_dedudes) p_arg_2,
        (name) p_arg_2, ".";
#EndIf;
#Ifndef OPTIONAL_NO_DARKNESS;
#Ifndef SKIP_MSG_EXAMINE_DARK;
    MSG_EXAMINE_DARK, MSG_SEARCH_DARK:
        "Mais vous êtes dans l'obscurité.";
#Endif;
#Endif;
#Iftrue MSG_SCORE_DEFAULT < 1000;
    MSG_SCORE_DEFAULT:
#Ifdef NO_SCORE;
        "Il n'y a pas de score dans ce jeu.";
        !de système de
#Ifnot;
        if (deadflag) print "Votre score final est de ";
        else print "Votre score est de ";
        print score, " sur ", MAX_SCORE, ", en ", turns, " tour";
        if (turns > 1) print "s";
        rtrue;
#Endif;
#Endif;
#IfDef OPTIONAL_FULL_SCORE;
#IfTrue MSG_FULLSCORE_START < 1000;
    MSG_FULLSCORE_START:
        print "Le score ";
        if(deadflag) print "était";
        else print "est";
        " composé de :"; !TODO
#EndIf;
#IfTrue MSG_FULLSCORE_END < 1000;
    MSG_FULLSCORE_END:
        print "point";
        if (score > 1) print "s";
        " (sur un total de ", MAX_SCORE, ")";
#EndIf;
#EndIf;
#Ifndef SKIP_MSG_LOOKMODE;
    MSG_LOOKMODE_NORMAL, MSG_LOOKMODE_LONG, MSG_LOOKMODE_SHORT:
        print "Ce jeu est maintenant dans son mode ";
        if(lookmode==1) print "normal « ";
        if(lookmode==2) print "« long";
        else {
            if(lookmode==3) print "« court";
            else print "bref";
        }
        print " », qui donne ";
        if(lookmode ~= 1) print "toujours ";
        print "de ";
        if(lookmode == 3) print "courtes";
        else print "longues";
        print " descriptions des lieux ";
        if(lookmode == 1)
            "jamais visités et de courtes descriptions dans les autres cas.";
        print "(même si vous ";
        if(lookmode  == 3) print "n'";
        print "y êtes ";
        if(lookmode  == 3) print "jamais";
        else print "déjà";
        " allé(e)).";
#Endif;
#IfTrue MSG_RESTART_RESTORE_OR_QUIT < 1000;
    MSG_RESTART_RESTORE_OR_QUIT:
        print "^Voulez-vous RECOMMENCER, CHARGER une partie sauvegardée";
#Ifdef OPTIONAL_PROVIDE_UNDO_FINAL;
    #Ifdef DEATH_MENTION_UNDO;
            if(((HDR_GAMEFLAGS->1) & 16) ~= 0)
                print ", ANNULER la dernière commande";
    #Ifnot;
            if(((HDR_GAMEFLAGS->1) & 16) ~= 0 && deadflag ~= GS_WIN)
                print ", ANNULER la dernière commande";
    #Endif;
#Endif;
#IfDef OPTIONAL_FULL_SCORE; !TODO
        print ", voir votre score TOTAL";
#EndIf;
        if(AMUSING_PROVIDED == 0 && deadflag == 2)
            print ", voir quelques actions BONUS amusantes";
        print " ou QUITTER ? ";
        rtrue;
#EndIf;
#IfTrue MSG_AREYOUSUREQUIT < 1000;
    MSG_AREYOUSUREQUIT: ! print and rtrue to avoid newline
        print "Êtes-vous sûr de vouloir interrompre la partie en cours ? ";
        rtrue;
#EndIf;
#IfTrue MSG_YOU_HAVE_WON < 1000;
    MSG_YOU_HAVE_WON: ! print and rtrue to avoid newline
        print "Vous avez gagné";
        rtrue;
#EndIf;
#IfTrue MSG_YOU_HAVE_DIED < 1000;
    MSG_YOU_HAVE_DIED: ! print and rtrue to avoid newline
        print "Vous êtes mort";
        rtrue;
#EndIf;
#IfTrue MSG_ENTER_BAD_LOCATION < 1000;
    MSG_ENTER_BAD_LOCATION:
        print "Vous devez d'abord ";
        if(player notin location && ~~IndirectlyContains(parent(player), noun)) {
            if (parent(player) has supporter) print "descendre";
            else print "sortir";
            print_ret (_dedudes) parent(player), (name) parent(player), ".";
        }
        print_ret "entrer dans ", (the) parent(noun), ".";
#EndIf;
#IfTrue MSG_ENTER_HELD < 1000; !TODO
    MSG_ENTER_HELD:
        "Vous ne pouvez pas entrer dans ", (the) noun, " en", (_lesl) noun, "ayant avec vous.";
        !TODO aller dans/sur ?
#EndIf;
#Ifndef SKIP_MSG_INSERT_NOT_CONTAINER;
#ifdef MSG_EMPTY_NOT_CONTAINER;
    MSG_INSERT_NOT_CONTAINER, MSG_EMPTY_NOT_CONTAINER:
#Ifnot;
    MSG_INSERT_NOT_CONTAINER:
#Endif;
        ! p_arg_1 = the object that can't contain things
        print (The) p_arg_1, " ne peu";
        if (p_arg_1 has pluralname) print "ven";
        "t rien contenir.";
#Endif;
#IfTrue MSG_YES_OR_NO < 1000;
    MSG_YES_OR_NO:
        print "Répondez par oui ou non : ";
        !, s'il vous plaît,
#EndIf;
#IfTrue MSG_RESTART_CONFIRM < 1000;
    MSG_RESTART_CONFIRM:
        print "Êtes-vous sûr de vouloir recommencer ? ";
#Endif;

#Ifndef NO_SCORE;
#Ifndef SKIP_MSG_NOTIFY_ON;
    MSG_NOTIFY_ON, MSG_NOTIFY_OFF:
        print "Notification du score ";
        if(p_msg == MSG_NOTIFY_OFF) print "dés";
        "activée.";
#Endif;
#Iftrue MSG_PARSER_NEW_SCORE < 1000;
    MSG_PARSER_NEW_SCORE:
        ! p_arg_1 = the old score
        print "^[Votre score vient ";
        if(p_arg_1 < score) {
            p_arg_2 = score - p_arg_1;
            print "d'augmenter";
        } else {
            p_arg_2 = p_arg_1 - score;
            print "de diminuer";
        }
        print " de ", p_arg_2, " point";
        if(p_arg_2 > 1) print "s";
        print ".]^";
#Endif;
#Endif;

#IfDef OPTIONAL_EXTENDED_VERBSET;
#IfTrue MSG_BLOW_DEFAULT < 1000;
    MSG_BLOW_DEFAULT:
        "Il ne se produit rien de particulier.";
        !Cela n'aboutirait pas à grand-chose.
        !"You can't usefully blow ", (the) noun, ".";
#EndIf;
#IfTrue MSG_EMPTY_ALREADY_EMPTY < 1000;
    MSG_EMPTY_ALREADY_EMPTY:
        ! p_arg_1 = the object that is already empty
        print_ret (CObjIs) p_arg_1, "déjà vide", (_s) p_arg_1, ".";
#EndIf;
#IfTrue MSG_SET_DEFAULT < 1000;
    MSG_SET_DEFAULT:
        "Ce n'est pas quelque chose que vous pouvez ", (verbName) verb_word, ".";
        !"Non, vous ne pouvez pas modifier cela."; !régler/modifier
#EndIf;
#IfTrue MSG_SET_TO_DEFAULT < 1000;
    MSG_SET_TO_DEFAULT:
        "Vous ne pouvez pas", (thatorthose) noun, "régler sur quoi que ce soit.";
#EndIf;
#IfTrue MSG_WAVE_DEFAULT < 1000;
    MSG_WAVE_DEFAULT:
        "Vous avez l'air un peu ridicule à brandir ", (the) noun, ".";
        !agiter
#EndIf;
#EndIf;

default:
        ! No code found. Print an error message.
        _RunTimeError(ERR_UNKNOWN_MSGNO);
    }
];

[ _s obj;
    if (obj has pluralname)
        @print_char 's';
];

[ _es obj;
    if (obj has female) @print_char 'e';
    if (obj has pluralname) @print_char 's'; !_s(obj);
];

[ _lesl obj;
    if (obj has pluralname) print " les ";
    else print " l'";
];

[ _nt obj;
    if (obj has pluralname) print "nt";
    @print_char ' ';
];

[ _dedudes obj;
    @print_char ' ';
    if (obj has pluralname) { print "des "; return; }
    if (obj has proper) { print "de "; return; }
    if (elide(obj)) { print "de l'"; return; }
    if (obj has female) print "de la ";
    else print "du ";
];

[ _aaux obj;
    @print_char ' ';
    if (obj has pluralname) { print "aux "; return; }
    if (obj has proper) { print "à "; return; }
    if ( elide(obj) ) { print "à l'"; return; }
    if (obj has female) print "à la ";
    else print "au ";
];

[ CThatorThose obj; !neuter Ce C' Ça
    if (obj == player) { print "Vous"; return; }
    if (obj has female) print "Elle";
    else print "Il";
    print (_s) obj;
];

[ CTheyreorThats obj;
    print (CThatorThose) obj, " ", (IsorAre) obj;
];

[ CTheyreorThatsNeg obj;
    print (CThatorThose) obj, " ", (IsorAreNeg) obj;
];

[ ThatorThose obj;
    @print_char ' ';
    if (obj == player) { print "vous "; return; }
    if (obj has pluralname) { print "les "; return; }
    if (obj has female) print "la ";
    else print "le ";
];

[ CObjIs obj;
    print (The) obj, " ", (IsOrAre) obj;
];

[ CObjIsNeg obj;
    print (The) obj, " ", (IsorAreNeg) obj;
];

[ IsorAreNeg obj;
    if (obj hasnt pluralname || obj == player)
        print "n'";
    else
        print "ne ";
    print (IsOrAre) obj, "pas ";
];

[ IsorAre obj;
    if (obj == player) {
        print "êtes ";
        return;
    }
    if (obj has pluralname)
        print "sont ";
    else
        print "est ";
];

[ OnOff obj;
    if(obj has on) print "allumé"; !éclairé
    else print "éteint";
    print (_es) obj;
];

!
! Error messages
!
Constant ERR_TOO_MANY_TIMERS_DAEMONS 1;
Constant ERR_OBJECT_HASNT_PROPERTY 2;
Constant ERR_SCOPE_FULL 3;
Constant ERR_UNKNOWN_MSGNO 4;
Constant ERR_INVALID_DIR_PROP 5;
Constant ERR_TOO_MANY_FLOATING 6;
Constant ERR_NOT_DIR_PROP 7;
Constant ERR_NOT_FAKE_OBJ 8;
Constant ERR_ILLEGAL_CHOOSEOBJNO 9;
Constant ERR_BUFFER_OVERRUN 10;

[_RunTimeError p_err p_obj _parent;
    print "^[PunyInform error: ";
    if(p_err ofclass string)
        print (string) p_err;
    else {
        print p_err;
#IfTrue RUNTIME_ERRORS == RTE_VERBOSE;
        print " - ";
        switch(p_err) {
        ERR_TOO_MANY_TIMERS_DAEMONS:
            print "Too many timers/daemons";
        ERR_OBJECT_HASNT_PROPERTY:
            print "Object lacks required property";
        ERR_SCOPE_FULL:
            print "Scope full";
        ERR_UNKNOWN_MSGNO:
            print "Unknown message#";
        ERR_INVALID_DIR_PROP:
            print "GoSub called with invalid direction property";
        ERR_TOO_MANY_FLOATING:
            print "Too many floating objects";
        ERR_NOT_DIR_PROP:
            print "DirPropToFakeObj called with non-dirprop";
        ERR_NOT_FAKE_OBJ:
            print "FakeObjToDirProp called with non-fakeobj";
        ERR_ILLEGAL_CHOOSEOBJNO:
            print "ChooseObjectsFinal_(Pick or Discard) called with nonexistent array index";
#Ifdef DEBUG;
        ERR_BUFFER_OVERRUN:
            print "Buffer overrun: Printing too many characters to a buffer";
#Endif;
        default:
            print "Unknown error";
        }
#EndIf;
    }
    print "]^";
#IfTrue RUNTIME_ERRORS == RTE_VERBOSE;
    if(p_obj ofclass object) {
        _parent = parent(p_obj);
        print "Offending object: ", (the) p_obj, " (", p_obj, ") in ", (name) _parent, " (", _parent, ")^";
    }
#Ifnot;
    _parent = p_obj; ! Avoid compiler warning
#EndIf;
    rtrue;
];
