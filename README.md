Adaptation et traduction de la bibliothèque **PunyInform 5.x** en français.

Version 3.0-dev. PunyInform 5.12   
Lionel Ange, 2020-2025.  
Licence Creative Commons BY-SA 4.0 (CC BY-SA 4.0)  
https://creativecommons.org/licenses/by-sa/4.0/deed.fr

**PunyInform** est une bibliothèque écrite en Inform 6, par Johan Berntsson et Fredrik Ramsberg, permettant de créer des jeux d’aventure textuels – ou fictions interactives –, en utilisant la machine virtuelle Z-machine. Ces jeux sont potentiellement jouables sur des ordinateurs 8-bit ainsi que sur des plateformes récentes.

**Nouvelle traduction et adaptation en cours de la bibliothèque PunyInform à partir de la version 5.**  
Seules les versions z5 et z8, de la Z-Machine, sont actuellement prises en charge par cette adaptation.  
La version 3 (.z3) est très contraignante, austère, et peu adaptée à la langue française. Les versions (obsolètes) 1 et 2 présentes dans ce dépôt supportent ce mode.

Le fichier source du jeu doit être encodé en **UTF-8**. Le compilateur Inform supporte nativement cet encodage avec l'option **-Cu**.

En-tête de fichier par défaut :
```
!% -Cu
!% -~S
!% $OMIT_UNUSED_ROUTINES=1
```
**Les accents**  
La saisie du joueur ainsi que les mots du dictionnaire doivent être accentués.  

**Les articles**  
La propriété *article* d’un objet a été modifiée pour être compatible avec la langue française. Elle doit être initialisée avec l’une des constantes suivantes en fonction du genre et du nombre de l’objet (ne pas mettre de propriété *article* pour un nom propre). Elle peut aussi être définie explicitement dans l'objet par une chaîne de caractères ou par une fonction.  
```
ARTICLE_UNE   la/une pomme
ARTICLE_UN    le/un dé
ARTICLE_DES   les/des lunettes
ARTICLE_DE_LA la/de la confiture
ARTICLE_DU    le/du sucre
ARTICLE_UNE_  l’/une orange
ARTICLE_UN_   l’/un œuf
ARTICLE_DE_L  l’/de l'eau
```
Les attributs de l'objet doivent être correctement initialisés en fonction du genre et du nombre : *proper; female; pluralname*. L'attribut *male* n'existe pas.
```
    Object pomme "pomme"
    with
        name 'pomme',
        article ARTICLE_UNE
    has edible female;
```
Vous pouvez rajouter une ou plusieurs entrées aux articles en redéfinisant la table *LanguageArticles* dans le code source de votre jeu :
```
Include "globals.h";
Constant ARTICLE_HORDE 24; !(3*8)
Array LanguageArticles static -->
    "La "   "la "  "une "   ! 0 pomme
    "Le "   "le "  "un "    ! 1 dé
    "Les "  "les " "des "   ! 2 lunettes
    "La "   "la "  "de la " ! 3 confiture (en)
    "Le "   "le "  "du "    ! 4 sucre     (en)
    "L'"    "l'"   "une "   ! 5 orange
    "L'"    "l'"   "un "    ! 6 œuf
    "L'"    "l'"   "de l'"  ! 7 eau
    "Les "  "les " "une horde de "  ! 8 brigands
;!End_Array
Include "puny.h";

Object brigands "brigands"
    with
        name 'brigands',
        article ARTICLE_HORDE
    has pluralname;
```
Il est aussi possible de récupérer la forme de l'article en paramètre de la propriété *article* :
```
Object brigands "brigands"
    with
        name 'brigands',
        article [ p_form;
            if (p_form == FORM_CDEF)
                print "Les ";
            else if (p_form == FORM_DEF)
                print "les ";
            else !FORM_INDEF
                print "une horde de ";
        ]
    has pluralname;
```    
**Les pronoms**  
La gestion des pronoms a été simplifiée par rapport à la bibliothèque standard 6.12 ; seuls les pronoms situés avant le verbe sont admis :
```
Le/la/les/l’          prendre
Le/la/les/l’ lui/leur donner
Lui/leur              donner la pomme
Le/la/les/l’          donner à Julia
s’asseoir
se lever
```
Le/la/les/l’ concernent les objets inanimés ; lui/leur, les objets ayant l’attribut *animate*.  
L’attribut *neuter*, associé à un objet, permet d’attribuer les pronoms le/la/les/l’ aux objets animés ou inanimés.

Il est possible d'associer directement un objet à un objet pronom : le_obj, la_obj, l_obj, les_obj, lui_obj, leur_obj.
```
la_obj = pomme;
l_obj  = pomme;
```
La forme impérative n’est pas supportée :  
`> Julia, prends la pomme`

La constante **CHAR_PROMPT** permet de supprimer le caractère espace après le signe > et avant la saisie.

La constante **MSG_OK** permet de définir un même message (ex : D'accord.) pour les constantes MSG_TAKE_DEFAULT, MSG_DROP_DROPPED, 
et MSG_REMOVE_DEFAULT.

La constante **OPTIONAL_TALK_TO** permet d’activer la grammaire du verbe *parler à qqn* sans la nécessité d’avoir en complément un sujet de conversation.

La constante **OPTIONAL_LOOKUNDER** permet d’activer la grammaire du verbe *regarder sous qqch*.

**Documentation**  
https://github.com/johanberntsson/PunyInform/tree/master/documentation  
http://www.inform-fiction.org/index.html

**Dépôts**  
Bibliothèque originale en anglais :  
https://github.com/johanberntsson/PunyInform  
Traduction de la bibliothèque en français :  
https://gitlab.com/auraes/punyinformfr  

**Crédits**  
PunyInform a été conçu et réalisé par Johan Berntsson et Fredrik Ramsberg. Le codage a été réalisé par Johan Berntsson, Fredrik Ramsberg, Pablo Martinez et Tomas Öberg. Inclus du code de la bibliothèque standard Inform 6, par Graham Nelson.
La traduction et l’adaptation en français ont été réalisées par Lionel Ange.

